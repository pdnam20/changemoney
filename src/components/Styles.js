import {
    StyleSheet,
    Dimensions,
  } from 'react-native';

const styles = StyleSheet.create({
    container: {
      width: 150, height: 45,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
     
    },
    input: {
        backgroundColor: 'tomato',
        flex:1,
        width: 150, height: 45,
    },
    buttonLabel: {
      padding: 10,
      textAlign: "center",
      justifyContent: 'center',
      alignItems: 'center',
      paddingTop: 13,
      paddingBottom: 13,
      fontSize: 20,
      color: "#35B0F1"
    },
    button: {
      width: "33.333333333%",
    },
    wave:{
      width: Dimensions.get("screen").width,
      height: 52,
     
  },
  });
  const StyleChange = StyleSheet.create({
    textinput : {
        
        width: 150,
        height: 40,
        textAlign:"right",
        textAlignVertical: 'center',
        fontSize: 22,
        color: "white"
    },
    keybroad: {
        flex: 3,
    },
    content:{
        flex: 5,
        
    },
    wave:{
        width: Dimensions.get("screen").width,
        height: 52,
       
    },
    from: {
        paddingLeft: 20,
        flex: 1,
        flexDirection: "row",
        alignItems: "center"
    },
    to:{
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        paddingLeft: 20,
    },
    line:{
        marginLeft: 20,
        width: 250,
        height: 2
        
    },
    viewline:{
        flexDirection: "row",
        alignItems: "center"
    },
    button:{
        width: 50,
        height: 50,
        backgroundColor: "#FFFFFF",
        justifyContent: "center",
        alignItems: "center",
        marginLeft: 15,
        paddingTop: 4,
        borderRadius: 50
       
    },
    row:{
        flex: 1,
        flexDirection: "row",
    },
    colunm:{
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    btnfont:{
        fontSize: 25,
        color: "#35B0F1"
    },
    headers:{
        flex: 1,
        backgroundColor:"blue",
        flexDirection: 'row',
        paddingHorizontal: 15
    }
})

  export {
      styles,
      StyleChange
  }