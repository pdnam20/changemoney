import React, { Component } from 'react';
import { View, Text, Picker, StyleSheet ,TouchableOpacity,Dimensions,Image,StatusBar} from 'react-native'
import {ScreenChange} from '../../ScreenName';
import ChangeScreen from './ChangeScreen';
import LinearGradient from 'react-native-linear-gradient';
export default class StartScreen extends Component {    


    static navigationOptions = {
        header: null
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
                <LinearGradient colors={['#35B0F1', '#1ECEFE']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                style = {{flex: 1, justifyContent: "center", alignItems: "center"}}>
                 <LinearGradient colors={['#35B0F1', '#1ECEFE']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                style = {{flex: 1,}}>
                    <StatusBar translucent ={true} backgroundColor={'transparent'} ></StatusBar>
                </LinearGradient>
                    
                    <View style = {{flex:6, justifyContent: "center", alignItems: "center"}}>
                        <Image source = {require('../images/logo.png')}></Image>
                        <Text style = {{fontSize: 40, color: "#F9FFFD"}}>Kurrency</Text>
                        <Text style = {{fontSize: 16, color: "#F9FFFD"}}>Easy Exchange</Text>
                    </View>
                    <View style = {{height: 40}}>
                        <Image source = {require('../images/loading.png')}></Image>
                    </View>
                    <View style = {StyleStart.btnview}>
                            <TouchableOpacity style = {StyleStart.button}
                            onPress = {() => {navigate(ScreenChange)}}
                            >
                                <Text style = {{
                                     fontSize: 25,
                                    color: "#35B0F1"
                                }}>Get Start</Text>
                            </TouchableOpacity>
                    </View>
                </LinearGradient>
            
        )
    }
}
const StyleStart = StyleSheet.create({
    button:{
        justifyContent: 'center',
        alignItems: 'center',
        width: 100,
        height: 50,
    },
    btnview:{
        flex: 1,
        backgroundColor:"#F9FFFD",
        width: Dimensions.get('screen').width,
        justifyContent:"center",
        alignItems: "center"
    }
})