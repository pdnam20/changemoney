import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, TouchableOpacity  } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import IconArrow from 'react-native-vector-icons/AntDesign';

export default class Header extends Component {
    render() {
        return (
            <View>
                <StatusBar translucent={true} backgroundColor={'transparent'} />
                <LinearGradient start={{ x: 0.8, y: 0 }} end={{ x: 0, y: 0 }} colors={['#88c240', '#126d38']} style={styles.statusBar} />
                <LinearGradient start={{ x: 1.2, y: 0 }} end={{ x: 0, y: 0 }} colors={['#126d38', '#88c240']} style={styles.header}>
                    <View style={{ flex: 1 }}>
                        <TouchableOpacity style={styles.iconBack} onPress = {() =>{this.props.comeBack()}}>
                            <IconArrow color='#fff' name="arrowleft" size={25} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.viewText}>
                        <Text style={styles.textHeader}>{this.props.title}</Text>
                    </View>
                    <View style={{ flex: 1 }}></View>
                </LinearGradient>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    statusBar: {
        height: StatusBar.currentHeight,
    },
    header: {
        flexDirection: 'row',

        alignItems: 'center',
        height: StatusBar.currentHeight * 2
    },
    textHeader: {
        color: '#fff',
        fontSize: 13
    },
    viewText: {
        flex: 1,
        alignItems: "center"
    },
    iconBack: {
        marginLeft: 10
    }
})
