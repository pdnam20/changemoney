import React, { Component } from 'react'
import {createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import StartScreen from './StartScreen'
import ChangeScreen from './ChangeScreen'
const rounter = createStackNavigator({
    Start: {
        screen: StartScreen
    },
    Change: {
        screen: ChangeScreen
    }
})
const Rounter = createAppContainer(rounter);
export default Rounter;
