import React, { Component } from 'react'
import {View,Text,TextInput,StyleSheet,TouchableOpacity,Alert,Keyboard,Image,Dimensions,StatusBar} from 'react-native'
import PickItem from './PickItem';
import {styles,StyleChange} from './Styles'
import LinearGradient from 'react-native-linear-gradient';


export default class ChangeScreen extends Component {
      static navigationOptions = {
        header: null
    }
    constructor(){
        super()
        this.state = {
            isLoading: true,
            data: "hello",
            text_1: '',
            text_2: '',  
            textvalue:"", 
            changText1: 'false',
            changeText2: 'false',
            valuechange:''         
        }
    }

    componentDidMount(){
        return fetch('https://api.exchangeratesapi.io/latest')
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                isLoading: false,
                data: responseJson.rates,
                
              }, function(){
      
              });
              console.log(this.state.data);
            })
            .catch((error) =>{
              console.log("done");
            });
        }
        
        onPress1 = () => {
          this.setState({
            textvalue: this.state.textvalue + "1"
          })
        }
        onPress2 = () => {
          this.setState({
            textvalue: this.state.textvalue + "2"
          })
        }
        onPress3 = () => {
          this.setState({
            textvalue: this.state.textvalue + "3"
          })
        }
        onPress4 = () => {
          this.setState({
            textvalue: this.state.textvalue + "4"
          })
        }
        onPress5 = () => {
          this.setState({
            textvalue: this.state.textvalue + "5"
          })
        }
        onPress6 = () => {
          this.setState({
            textvalue: this.state.textvalue + "6"
          })
        }
        onPress7 = () => {
          this.setState({
            textvalue: this.state.textvalue + "7"
          })
        }
        onPress8 = () => {
          this.setState({
            textvalue: this.state.textvalue + "8"
          })
        }
        onPress9 = () => {
          this.setState({
            textvalue: this.state.textvalue + "9"
          })
        }
        onPress_dop = () => {
          this.setState({
            textvalue: this.state.textvalue + "."
          })
        }
        onPress0 = () => {
          this.setState({
            textvalue: this.state.textvalue + "0"
          })
        }
        onPress_backspace = () => {
          let l = this.state.textvalue.length;
          this.setState({
            
            textvalue: this.state.textvalue.substr(0,l-1),
          })
          console.log(this.state.textvalue);
        }
      calculate = () => {
        
        let temp1 = this.refs.Pick1.getstate(); 
        let temp2 = this.refs.Pick2.getstate();
       
        console.log(this.state.data[temp1]);
        console.log(this.state.data[temp2]);
        console.log((this.state.textvalue / this.state.data[temp1]) * this.state.data[temp2])
        
        this.setState({
          valuechange:((this.state.textvalue / this.state.data[temp1]) * this.state.data[temp2]).toFixed(4)
        })
      }
   
    

    render() {
        return (        
           <View style = {{flex: 1,}}>
              <LinearGradient colors={['#35B0F1', '#1ECEFE']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                style = {{flex: 5}}>
                <LinearGradient colors={['#35B0F1', '#1ECEFE']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                style = {{flex: 1,}}>
                    <StatusBar translucent ={true} backgroundColor={'transparent'} ></StatusBar>
                </LinearGradient>
              <View style = {StyleChange.headers}>
                <Image></Image>
              </View>
              <View style = {StyleChange.content}>
                  <View style = {StyleChange.from}>
                      <PickItem ref = "Pick1"/>
                      <Text style = {StyleChange.textinput}
                      >{this.state.textvalue}</Text>
                      {/* Kí tự tiền tệ */}
                      <Text></Text>
                  </View>

                  <View style = {StyleChange.viewline}>
                      <Image style = {StyleChange.line}
                      source = {require('../images/line.png')}></Image>
                      
                      <TouchableOpacity style = {StyleChange.button}
                      onPress = {() => this.calculate()}>
                          <Image source = {require('../images/icon.png')}
                          style = {{flex: 1,paddingRight: 10}}
                          ></Image>
                      </TouchableOpacity>
                  </View>

                  <View style = {StyleChange.to}>
                      <PickItem ref = "Pick2"/>
                      <Text style = {StyleChange.textinput}>
                        {this.state.valuechange}
                      </Text>
                      {/* Kí tự tiền tệ */}
                      <Text></Text>
                  </View>
                </View>
             
                {/* Gợn sóng */}
                <View style = {{height: 50}}>
                    <Image style = {StyleChange.wave}
                        source = {require('../images/loading.png')}></Image>
                </View>
                </LinearGradient>
                {/* Bàn Phím */}
                <View style = {StyleChange.keybroad}>
                    <View style = {StyleChange.row}>
                        <View style = {StyleChange.colunm}>
                            <TouchableOpacity style = {{flex: 1}}
                            onPress = {() => this.onPress1()}>
                            <Text style = {StyleChange.btnfont}>1</Text>
                            </TouchableOpacity>
                        </View>
                        <View style = {StyleChange.colunm}>
                            <TouchableOpacity style = {{flex: 1}}
                             onPress = {() => this.onPress2()}>
                            <Text style = {StyleChange.btnfont}>2</Text>
                            </TouchableOpacity>
                        </View>
                        <View style = {StyleChange.colunm}>
                            <TouchableOpacity style = {{flex: 1}}
                             onPress = {() => this.onPress3()}>
                            <Text style = {StyleChange.btnfont}>3</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style = {StyleChange.row}>
                        <View style = {StyleChange.colunm}>
                            <TouchableOpacity style = {{flex: 1}}
                            onPress = {() => this.onPress4()}>
                            <Text style = {StyleChange.btnfont}>4</Text>
                            </TouchableOpacity>
                        </View>
                        <View style = {StyleChange.colunm}>
                            <TouchableOpacity style = {{flex: 1}}
                            onPress = {() => this.onPress5()}>
                            <Text style = {StyleChange.btnfont}>5</Text>
                            </TouchableOpacity>
                        </View>
                        <View style = {StyleChange.colunm}>
                            <TouchableOpacity style = {{flex: 1}}
                            onPress = {() => this.onPress6()}>
                            <Text style = {StyleChange.btnfont}>6</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style = {StyleChange.row}>
                        <View style = {StyleChange.colunm}>
                            <TouchableOpacity style = {{flex: 1}}
                            onPress = {() => this.onPress7()}>
                            <Text style = {StyleChange.btnfont}>7</Text>
                            </TouchableOpacity>
                        </View>
                        <View style = {StyleChange.colunm}>
                            <TouchableOpacity style = {{flex: 1}}
                            onPress = {() => this.onPress8()}>
                            <Text style = {StyleChange.btnfont}>8</Text>
                            </TouchableOpacity>
                        </View>
                        <View style = {StyleChange.colunm}>
                            <TouchableOpacity style = {{flex: 1}}
                            onPress = {() => this.onPress9()}>
                            <Text style = {StyleChange.btnfont}>9</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style = {StyleChange.row}>
                        <View style = {StyleChange.colunm}>
                            <TouchableOpacity style = {{flex: 1}}
                            onPress = {() => this.onPress_dop()}>
                            <Text style = {StyleChange.btnfont}>.</Text>
                            </TouchableOpacity>
                        </View>
                        <View style = {StyleChange.colunm}>
                            <TouchableOpacity style = {{flex: 1}}
                            onPress = {() => this.onPress0()}>
                            <Text style = {StyleChange.btnfont}>0</Text>
                            </TouchableOpacity>
                        </View>
                        <View style = {StyleChange.colunm}>
                            <TouchableOpacity style = {{flex: 1}}
                            onPress = {() => this.onPress_backspace()}>
                                <Image source = {require('../images/Back.png')}></Image>
                            </TouchableOpacity> 
                        </View>
                    </View>
                </View>
           </View>
        )
    }
}
