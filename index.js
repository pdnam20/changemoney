/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import App from './App';
import {name as appName} from './app.json';
import ChangeScreen from './src/components/ChangeScreen'
import StartScreen from './src/components/StartScreen.js'
import {ScreenStart,ScreenChange}from './ScreenName'
import {createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import NewTextInput from './src/components/NewTextInput'
const rounter = createStackNavigator({
    
    ScreenStart: {
        screen: StartScreen
    },
    
    ScreenChange: {
        screen: ChangeScreen
    }
})
const App = createAppContainer(rounter);


AppRegistry.registerComponent(appName, () =>App);
